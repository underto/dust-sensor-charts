<?php
#
# Get JSON POST from SDS011 sensor, save into SQlite db.
# SDS011: http://inovafitness.com/en/a/chanpinzhongxin/95.html
#
# When $_POST it saves data, when $_GET it shows charts/data.
# Options: 
# * ?limit=n : how many samples are shown (hardlimit: 600)
# * ?out=json : data output only via json 

# Settings ################################

# SQlite db
define( 'SQLITE_DB', '/usr/local/www/sensors.db' );

# Page refresh time
define( 'REFRESH', '120' );

# Samples in charts/data
define( 'SHOW_SAMPLES', '25' );

# Proxy POST request to another URL (spoiler: it doesn't work)
define( 'POST_REPEAT_URL' , '' );

# Charts colors
define( 'PM10', '#ff0000' );
define( 'PM25', '#ff8000' );
define( 'TEMP', '#008800' );
define( 'HUM', '#00aaff' );
define( 'SMP', '#ccddee' );
define( 'MIN', '#feffd3' );
define( 'MAX', '#e2e8ee' );
define( 'WIFI', '#cccccc' );

# Timezone
date_default_timezone_set('Europe/Rome');

#################################################

function parse_timeline( &$item, $key ){
	$newitem = explode( ' ', $item );
	$hours = explode( ':', $newitem[1] );
	$item = '"' . $hours[0].':'.$hours[1] . '"';
}

function parse_labels( &$item, $key ){
	$item = '"' . $item . '"';
}

function prepare_labels( $item, $action ){
	$item = array_reverse($item);
	array_walk( $item, $action );
	return join( ',', $item );	
}

try{
	
	# link db
	$db = new PDO( 'sqlite:' . SQLITE_DB );
	$db->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );

	# tables
	$tbl_dati = 'CREATE TABLE IF NOT EXISTS dati (
		id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
		sds_p1 REAL,
		sds_p2 REAL,
		temp REAL,
		hum REAL,
		samples INTEGER,
		min_micro INTEGER,
		max_micro INTEGER,
		signal TEXT,
		date DATETIME DEFAULT CURRENT_TIMESTAMP
	)';
	
	$tbl_settings = 'CREATE TABLE IF NOT EXISTS settings (
	id INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,
	key TEXT,
	value TEXT
	)';
	
	# tables setup
	$db->exec( $tbl_dati );
	$db->exec( $tbl_settings );

	# main logic: POST request
	if ($_SERVER['REQUEST_METHOD'] === 'POST') {

		# read input
		$postdata = file_get_contents( 'php://input' );
		$rawdata = json_decode( $postdata, true );
		
		# prepare query
		$values = $rawdata['sensordatavalues'];
		$insert = "INSERT INTO dati (sds_p1, sds_p2, temp, hum, samples, min_micro, max_micro, signal ) VALUES ( :sds_p1, :sds_p2, :temp, :hum, :samples, :min_micro, :max_micro, :signal )";

		# parametrize query
		$dblink = $db->prepare( $insert );
		$dblink->bindParam( ':sds_p1', $values[0]['value'] );
		$dblink->bindParam( ':sds_p2', $values[1]['value'] );
		$dblink->bindParam( ':temp', $values[2]['value'] );
		$dblink->bindParam( ':hum', $values[3]['value'] );
		$dblink->bindParam( ':samples', $values[4]['value'] );
		$dblink->bindParam( ':min_micro', $values[5]['value'] );
		$dblink->bindParam( ':max_micro', $values[6]['value'] );
		$dblink->bindParam( ':signal', $values[7]['value'] );

		# run query
		$dblink->execute();

		# BROKEN: proxy POST request to another url
		if( POST_REPEAT_URL !== '' ):
			$ch = curl_init( POST_REPEAT_URL );
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS, $postdata);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			$response = curl_exec($ch);
			exec( 'logger ' . json_encode($response) );
			curl_close($ch);
		endif;
		
	
	} else {
		# main logic: GET request

		# define limit
		$limit = isset( $_GET['limit'] ) ? $_GET['limit'] : SHOW_SAMPLES;
		if( $limit > 600 ) $limit = 600;
		
		# query DB
		$search = $db->prepare( 'SELECT id, sds_p1, sds_p2, temp, hum, samples, min_micro, max_micro, signal, datetime(date, "localtime") as date FROM dati ORDER BY id DESC LIMIT :limit' );
		$search->bindParam( ':limit', $limit );
		$search->execute();
		
		# deal with output format
		switch( $_GET['out'] ):

			case "json":
				# setup data arrays
				while( $lastval = $search->fetch(\PDO::FETCH_ASSOC) ):
				  $i							= $lastval['date'];
				  $sdsp1[$i]			= $lastval['sds_p1'];
				  $sdsp2[$i]			= $lastval['sds_p2'];
				  $temp[$i]				= $lastval['temp'];
				  $hum[$i]				= $lastval['hum'];
				  $samples[$i]		= $lastval['samples'];
				  $min_micro[$i]	= $lastval['min_micro'];
				  $max_micro[$i]	= $lastval['max_micro'];
				  $signal[$i]			= $lastval['signal'];
				endwhile;
			
				$output = array(
					'sdsp1'			=> $sdsp1,
					'sdsp2'			=> $sdsp2,
					'temp'			=> $temp,
					'hum'				=> $hum,
					'samples'		=> $samples,
					'min_micro'	=> $min_micro,
					'max_micro'	=> $max_micro,
					'signal'		=> $signal,
				);
			
				# output json data
				echo json_encode( $output );

			break;

			case 'html':
			default:
				
				# setup data
				while( $lastval = $search->fetch(\PDO::FETCH_ASSOC) ):
				  $i							= $lastval['date'];
					$timeline[]			= $lastval['date'];
				  $sdsp1[$i]			= $lastval['sds_p1'];
				  $sdsp2[$i]			= $lastval['sds_p2'];
				  $temp[$i]				= $lastval['temp'];
				  $hum[$i]				= $lastval['hum'];
				  $samples[$i]		= $lastval['samples'];
				  $min_micro[$i]	= $lastval['min_micro'];
				  $max_micro[$i]	= $lastval['max_micro'];
				  $signal[$i]			= $lastval['signal'];
				endwhile;
			
				# setup labels
				$labels			= prepare_labels( $timeline, 'parse_timeline' );
				$val_pm10		= prepare_labels( $sdsp1, 'parse_labels' );
				$val_pm25		= prepare_labels( $sdsp2, 'parse_labels' );
				$val_temp		= prepare_labels( $temp, 'parse_labels' );
				$val_hum		= prepare_labels( $hum, 'parse_labels' );
				$val_smp		= prepare_labels( $samples, 'parse_labels' );
				$val_min		= prepare_labels( $min_micro, 'parse_labels' );
				$val_max		= prepare_labels( $max_micro, 'parse_labels' );
				$val_signal	= prepare_labels( $signal, 'parse_labels' );
				
				# render page
				?><html>
<head>
<meta http-equiv="refresh" content="<?php echo REFRESH; ?>"/>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.min.js"></script>
	<style>
		body{ text-align:center; }
		canvas{ background-color:#fff; }
		.chart{ position: relative; width:90vw; margin:2vw 0 2vw 2vw; border:1px solid #ddd; background-color:#eee; padding:1vw; }
	</style>
</head>
<body>

<h1>SDS011 Charts</h1>

<div class="chart"><canvas id="all"></canvas></div>

<div class="chart"><canvas id="aria"></canvas></div>
<div class="chart"><canvas id="room"></canvas></div>
<div class="chart"><canvas id="minmax"></canvas></div>
<div class="chart"><canvas id="signal"></canvas></div>

<script>
window.onload = function() {
	
	var data_pm10 = {
				type: 'line',
				label: 'PM 10',
				borderColor: '<?php echo PM10; ?>',
				backgroundColor: '<?php echo PM10; ?>',
				fill: '+1',
				pointRadius: 1,
				data:[<?php echo $val_pm10; ?>],
				yAxisID: "pm"
	}
	
	var data_pm25 = {
				type: 'line',
				label: 'PM 2.5',
				borderColor: '<?php echo PM25; ?>',
				backgroundColor: '<?php echo PM25; ?>',
				pointRadius: 1,
				fill: true,
				data:[<?php echo $val_pm25; ?>],
				yAxisID: "pm"
	}
	
	var data_temp = {
				type: 'line',
				label: 'Temperature',
				borderColor: '<?php echo TEMP; ?>',
				backgroundColor: '<?php echo TEMP; ?>',
				borderDash: [1,1],
				pointRadius: 1,
				pointHover: 10,
				fill: false,
				data:[<?php echo $val_temp; ?>],
				yAxisID: "temp"
	}
	
	var data_hum = {
				type: 'line',
				label: 'Humidity',
				borderColor: '<?php echo HUM; ?>',
				backgroundColor: '<?php echo HUM; ?>',
				borderDash: [1, 1],
				pointRadius: 1,
				pointHover: 10,
				fill: false,
				data:[<?php echo $val_hum; ?>],
				yAxisID: "hum"
	}

	var data_smp = {
				type: 'bar',
				label: 'Samples',
				borderColor: '<?php echo SMP; ?>',
				backgroundColor: '<?php echo SMP; ?>',
				fill: false,
				hidden: true,
				data:[<?php echo $val_smp; ?>],
				yAxisID: "smp"
	}
	
	var data_min = {
				type: 'bar',
				label: 'min_micro',
				borderColor: '<?php echo MIN; ?>',
				backgroundColor: '<?php echo MIN; ?>',
				borderDash: [1,1],
				fill: false,
				data:[<?php echo $val_min; ?>],
				yAxisID: "smp"
	}
	
	var data_max = {
				type: 'bar',
				label: 'max_micro',
				borderColor: '<?php echo MAX; ?>',
				backgroundColor: '<?php echo MAX; ?>',
				fill: false,
				hidden: true,
				data:[<?php echo $val_max; ?>],
				yAxisID: "smp"
	}
	
	var data_wifi = {
				type: 'line',
				label: 'WiFi strength ',
				borderColor: '<?php echo WIFI; ?>',
				backgroundColor: '<?php echo WIFI; ?>',
				fill: 'origin',
				steppedLine: 'after',
				pointRadius: 0,
				data:[<?php echo $val_signal; ?>],
				yAxisID: "wifi"
	}

	var chart0 = document.getElementById( 'all' ).getContext( '2d' );
	var allchart = new Chart( chart0, {
		type: 'bar',
		data: {
			labels: [<?php echo $labels; ?>],
			datasets: [ data_temp, data_hum, data_pm10, data_pm25, data_min, data_max, data_smp, data_wifi ]
		},
		options: {
			responsive: true,
			hoverMode: 'index',
			stacked: true,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{
					stacked: true,
					display: true,
				}],
				yAxes: [{
					type: 'linear',
					display: true,
					position: 'left',
					id: 'pm',
					scaleLabel: {
						display: true,
						labelString: 'µg/m³'
					}
				},{
					type: 'linear',
					display: true,
					position: 'right',
					id: 'temp',
					gridLines: {
						drawOnChartArea: false,
					},
					scaleLabel: {
						display: true,
						labelString: '°C'
					}
				},{
					type: 'linear',
					display: false,
					position: 'left',
					id: 'hum',
					gridLines: {
						drawOnChartArea: false,
					},
				},{
					type: 'linear',
					stacked: true,
					display: false,
					position: 'right',
					id: 'smp',
					gridLines: {
						drawOnChartArea: false,
					},
				},{
					type: 'linear',
					display: false,
					position: 'right',
					id: 'wifi',
					gridLines: {
						drawOnChartArea: false,
					},
				}]
			}
		}
	});

	var chart1 = document.getElementById( 'aria' ).getContext( '2d' );
	var ariachart = new Chart.Line( chart1, {
		data: {
			labels: [<?php echo $labels; ?>],
			datasets: [ data_pm10, data_pm25 ]
		},
		options: {
			tooltips:{
				mode: 'index',
				intersect: false,
				callbacks: {
					label: function( tooltipItem, data ){ 
						label = data.datasets[ tooltipItem.datasetIndex ].label
						value = data.datasets[ tooltipItem.datasetIndex ].data[ tooltipItem.index ];
						return label + ': ' + value + ' µg/m³';
					}
				}
			},
			responsive: true,
			hoverMode: 'index',
			stacked: false,
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					display: true,
					position: 'left',
					id: 'pm',
					scaleLabel: {
						display: true,
						labelString: 'µg/m³'
					}
				},{
					type: 'linear',
					display: true,
					position: 'left',
					id: 'pm',
					scaleLabel: {
						display: true,
						labelString: 'µg/m³'
					}
				}]
			}
		}
	});
	
	var chart2 = document.getElementById( 'room' ).getContext( '2d' );	
	var roomchart = new Chart.Line( chart2, {
		data: {
			labels: [<?php echo $labels; ?>],
			datasets: [ data_temp, data_hum ]
		},
		options: {
			tooltips:{
				mode: 'index',
				intersect: false,
				callbacks: {
					label: function( tooltipItem, data ){ 
						suffix = ( tooltipItem.datasetIndex == 0 ) ? '°C' : '%';
						label = data.datasets[ tooltipItem.datasetIndex ].label
						value = data.datasets[ tooltipItem.datasetIndex ].data[ tooltipItem.index ];
						return label + ': ' + value + suffix;
					}
				}
			},
			responsive: true,
			hoverMode: 'index',
			stacked: false,
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					type: 'linear',
					display: true,
					position: 'left',
					id: 'temp',
					scaleLabel: {
						display: true,
						labelString: '°C'
					}
				},{
					type: 'linear',
					display: true,
					position: 'right',
					id: 'hum',
					gridLines: {
						drawOnChartArea: false,
					},
					scaleLabel: {
						display: true,
						labelString: '%'
					}
				}]
			}
		}
	});

	var chart3 = document.getElementById( 'minmax' ).getContext( '2d' );	
	var minmaxchart = new Chart.Bar( chart3, {
		data: {
			labels: [<?php echo $labels; ?>],
			datasets: [ data_min, data_max, data_smp ]
		},
		options: {
			responsive: true,
			hoverMode: 'index',
			stacked: true,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{
					stacked: true,
					display: true,
				}],
				yAxes: [{
					stacked: true,
					type: 'linear',
					display: true,
					position: 'left',
					id: 'smp',
				}]
			}
		}
	});

	var chart4 = document.getElementById( 'signal' ).getContext( '2d' );	
	var signalchart = new Chart.Line( chart4, {
		data: {
			labels: [<?php echo $labels; ?>],
			datasets: [ data_wifi ],
		},
		options: {
			responsive: true,
			hoverMode: 'index',
			stacked: false,
			tooltips: {
				mode: 'index',
				intersect: false
			},
			scales: {
				xAxes: [{
					display: true,
				}],
				yAxes: [{
					type: 'linear',
					display: true,
					position: 'left',
					id: 'wifi',
					scaleLabel: {
						display: true,
						labelString: 'dB'
					}
				}]
			}
		}
	});
	
}
</script>

</body>
</html><?php
			break;

		endswitch;
	}

	# close SQlite link
	$db = null;

}catch(PDOException $e){
	echo $e->getMessage();
}
